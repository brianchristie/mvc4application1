﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mvc4Application1.Models
{
    public class HomeModel
    {
        public String CurrentTime()
        {
            return DateTime.Now.ToString();
        }
    }
}
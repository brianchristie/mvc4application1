﻿using Mvc4Application1.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace Mvc4TestProject1
{
    
    
    /// <summary>
    ///This is a test class for HomeModelTest and is intended
    ///to contain all HomeModelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HomeModelTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for HomeModel Constructor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        //[HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost("C:\\Projects\\Mvc4Application1\\Mvc4Application1", "/")]
        //[UrlToTest("http://localhost:54288/")]
        public void HomeModelConstructorTest()
        {
            HomeModel target = new HomeModel();
            //Assert.Inconclusive("TODO: Implement code to verify target");
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for CurrentTime
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        //[HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost("C:\\Projects\\Mvc4Application1\\Mvc4Application1", "/")]
        //[UrlToTest("http://localhost:54288/")]
        public void CurrentTimeTest()
        {
            HomeModel target = new HomeModel(); // TODO: Initialize to an appropriate value
            string expected = DateTime.Now.ToString(); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.CurrentTime();
            Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}

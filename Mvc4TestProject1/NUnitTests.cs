﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Mvc4Application1.Models;

namespace Mvc4TestProject1
{
    [TestFixture]
    class NUnitTests
    {
        [Test]
        public void HomeModel_CurrentTime()
        {
            HomeModel target = new HomeModel(); // TODO: Initialize to an appropriate value
            string expected = DateTime.Now.ToString(); // TODO: Initialize to an appropriate value
            string actual;
            actual = target.CurrentTime();
            Assert.AreEqual(expected, actual);
        }
    }
}

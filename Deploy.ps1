param($server, $username, $password, $webpath, $folder, $ftpurl)

#$server = "osmocloud08.osmotion.com.au"
#$username = "osmotion\brianc"
#$password = "XXXX"
#$webpath = "G:\inetpub\hostingroot\brian"
#$folder = "Mvc4Application1"
#$ftpurl = "ftp://brianc:XXXX@osmocloud03.osmotion.com.au"
#Leave ftpurl blank to use Web Deploy

#MSDeploy
$MSDeploy = "C:\Program Files\IIS\Microsoft Web Deploy V3\msdeploy"
$currentdir = [environment]::CurrentDirectory
$Source = Join-Path $currentdir $folder
$Dest = "-dest:dirPath=$webpath,computername=$server,username=$username,password=$password"
if (!$ftpurl) {
	& $MSDeploy "-verb:sync" "-source:dirPath=$Source" $Dest
}

else {
	#ZIP the files to FTP path
	$filename = "BuildFiles.zip"
	if (!$folder) { $filename = $folder + ".zip" }
	$ZIPfolder = Join-Path $currentdir "$folder\*"
	$ZIPfile = Join-Path "C:\inetpub\ftproot" $filename
	if (Test-Path $ZIPfile) { rm $ZIPfile }
	& "C:\Program Files\7-Zip\7z.exe" "a" "-tzip" $ZIPfile $ZIPfolder

	#Credentials for remote server
	$pass = ConvertTo-SecureString -AsPlainText $password -Force
	$psCred = New-Object System.Management.Automation.PSCredential -ArgumentList $username, $pass

	Invoke-Command -ComputerName $server -Credential $psCred -ArgumentList $webpath, $filename, $ftpurl –ScriptBlock { 
		param($webpath, $filename, $ftpurl)
		$webzip = Join-Path $webpath $filename
		rm "$webpath\*" -recurse

		#Get ZIP from FTP (can't get artifact from HTTP becuase need to be logged in to bamboo)
		$ftp = $ftpurl + "/" + $filename
		"ftp url: $ftp"
		$webclient = New-Object System.Net.WebClient
		$uri = New-Object System.Uri($ftp)
		"Downloading to $webzip..."
		$webclient.DownloadFile($uri, $webzip)

		#Unzip
		& "C:\Program Files\7-Zip\7z.exe" "x" $webzip "-o$webpath"
	}
}